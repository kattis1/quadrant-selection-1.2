import sys

counter = 0
x = False
y = False
for line in sys.stdin:
    if counter == 0:
        x = (float(line) > 0)
    else:
        y = (float(line) > 0)
    counter += 1
    if counter == 2:
        break

if x and y :
    print(1)
elif x and not y:
    print(4)
elif not x and not y:
    print(3)
elif not x and y:
    print(2)